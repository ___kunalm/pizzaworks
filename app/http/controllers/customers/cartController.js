const { default: axios } = require('axios');

function cartController() {
	return {
		index(req, res) {
			res.render('customers/cart');
		},
		update(req, res) {
			// let cart = {
			// 	items: {
			// 		pizzaId: { item: pizzaObject, qty: 0 },
			// 	},
			// 	totalQty: 0,
			// 	totalPrice: 0
			// }
			// console.log(req);

			if (!req.session.cart) {
				req.session.cart = {
					items: {},
					totalQty: 0,
					totalPrice: 0,
				};
			}

			let cart = req.session.cart;

			if (!cart.items[req.body._id]) {
				cart.items[req.body._id] = {
					item: req.body,
					qty: 1,
				};
				cart.totalQty += 1;
				cart.totalPrice += req.body.price;
			} else {
				cart.items[req.body._id].qty += 1;
				cart.totalQty += 1;
				cart.totalPrice += req.body.price;
			}

			return res.json({
				totalQty: req.session.cart.totalQty,
			});
		},
		deleteCart(req, res) {
			delete req.session.cart;
			return res.json({
				message: 'Cart items deleted.',
			});
		},
		updateQty(req, res) {
			let cart = req.session.cart;

			// console.log(cart.items[req.body.pid].item.price);

			if (req.body.op === 'plus') {
				cart.items[req.body.pid].qty += 1;
				cart.totalPrice += cart.items[req.body.pid].item.price;
			} else if (req.body.op === 'minus') {
				if (cart.items[req.body.pid].qty >= 1) {
					cart.items[req.body.pid].qty -= 1;
					cart.totalPrice -= cart.items[req.body.pid].item.price;
				}
			}

			if (cart.items[req.body.pid].qty === 0) {
				delete cart.items[req.body.pid];
			}

			if (
				cart.items &&
				Object.keys(cart.items).length === 0 &&
				cart.items.constructor === Object
			) {
				return res.json({
					message: 'delete cart',
				});
			}

			return res.json({
				message: 'success',
			});
		},
	};
}

module.exports = cartController;
