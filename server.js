//Imports
require('dotenv').config();
const express = require('express');
const ejs = require('ejs');
const mongoose = require('mongoose');
const expressLayout = require('express-ejs-layouts');
const path = require('path');
const session = require('express-session');
const flash = require('express-flash');
const MongoDbStore = require('connect-mongo');
const passport = require('passport');
const Emitter = require('events');

const initRoutes = require('./routes/web');

const app = express();

// Database Connection
const url = 'mongodb://localhost:27017/realtime-app';

mongoose.connect(url, {
	useNewUrlParser: true,
	useCreateIndex: true,
	useUnifiedTopology: true,
	useFindAndModify: true,
});

const connection = mongoose.connection;

connection
	.once('open', () => {
		console.log('Database connected...⚙️');
	})
	.catch((err) => {
		console.log('Connection failed...❎');
	});

// Session store
let mongoStore = new MongoDbStore({
	mongoUrl: url,
	mongooseConnection: connection,
	collection: 'sessions',
});

// Event Emitter

const eventEmitter = new Emitter();
app.set('eventEmitter', eventEmitter);

// Session config
app.use(
	session({
		secret: process.env.COOKIE_SECRET,
		resave: false,
		store: mongoStore,
		saveUninitialized: false,
		cookie: {
			maxAge: 1000 * 60 * 60 * 24,
		},
	})
);

// Passport config
const passportInit = require('./app/config/passport');
passportInit(passport);
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

// Assets
app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Global middleware
app.use((req, res, next) => {
	res.locals.session = req.session;
	res.locals.user = req.user;
	next();
});

// set Template engine
app.use(expressLayout);
app.set('views', path.join(__dirname, '/resources/views'));
app.set('view engine', 'ejs');

initRoutes(app);

const PORT = process.env.PORT || 3000;
const server = app.listen(PORT, () => {
	console.log(`Server listening on port ${PORT}`);
});

// Socket
const io = require('socket.io')(server);

io.on('connection', (socket) => {
	// console.log(socket.id);
	socket.on('join', (roomName) => {
		socket.join(roomName);
	});
});

eventEmitter.on('orderUpdated', (data) => {
	io.to(`order_${data.id}`).emit('orderUpdated', data);
});

eventEmitter.on('orderPlaced', (data) => {
	io.to('adminRoom').emit('orderPlaced', data);
});
