## Pizzaworks 🍕🍕

-   An app that allows users to choose their favorite pizza, receive real-time notifications about the pizza status.
-   This project is covered with EJS - TailwindCSS, Node.js, Express.js, MongoDB, Socket.io for routes and sockets
- live at: [pizzaworks](https://pizzaworks.herokuapp.com)

### Screenshots 📷

#### Homepage

![alt text](https://i.ibb.co/PYrfy4N/Screenshot-2021-06-18-at-14-24-51-Pizzaworks.png)
![alt text](https://i.ibb.co/qjZ5TWN/Screenshot-2021-06-18-at-14-25-06-Pizzaworks.png)

#### Login Page

![alt text](https://i.ibb.co/23NkCvZ/Screenshot-2021-06-18-at-14-25-59-Pizzaworks.png)

#### Orders page

![alt text](https://i.ibb.co/3B8bzSG/Screenshot-2021-06-18-at-14-26-13-Pizzaworks.png)

#### Delivery tracking page

![alt text](https://i.ibb.co/SR9rDLt/Screenshot-2021-06-18-at-14-29-05-Pizzaworks.png)

#### Admin page

![alt text](https://i.ibb.co/xG3bZJD/Screenshot-2021-06-18-at-14-37-17-Pizzaworks.png)
![alt text](https://i.ibb.co/LCVNdW3/Screenshot-2021-06-18-at-14-56-19-Pizzaworks.png)
![alt text](https://i.ibb.co/7C4n90T/Screenshot-2021-06-18-at-14-56-33-Pizzaworks.png)
