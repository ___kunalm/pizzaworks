import { loadStripe } from '@stripe/stripe-js';
import { placeOrder } from './apiService';
import { CardWidget } from './CardWidget';

async function callUtil(card, formObject) {
	const token = await card.createToken();
	formObject.stripeToken = token.id;
	placeOrder(formObject);
}

export async function initStripe() {
	const stripe = await loadStripe(
		'pk_test_51IGHkrLCVvvTxIhXu5WS755TUeccdmVJqKCHoxlBo3cI1v3jlspcbisTmBOqVhmpzHPvHkg8cxkvBYesE9kBrFOC00rBhNIdbe'
	);

	let card = null;

	const paymentType = document.querySelector('#payment-type');

	if (!paymentType) {
		return;
	}

	paymentType.addEventListener('change', (e) => {
		if (e.target.value === 'card') {
			// Display Widget
			card = new CardWidget(stripe);
			card.mount();
		} else {
			card.destroy();
		}
	});

	const paymentForm = document.querySelector('#payment-form');

	if (paymentForm) {
		paymentForm.addEventListener('submit', (e) => {
			e.preventDefault();

			let formData = new FormData(paymentForm);
			let formObject = {};

			for (let [key, value] of formData.entries()) {
				formObject[key] = value;
			}

			// Verify card
			if (!card) {
				placeOrder(formObject);
				return;
			}

			callUtil(card, formObject);
		});
	}
}
